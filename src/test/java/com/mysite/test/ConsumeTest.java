package com.mysite.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConsumeTest {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring-consumer.xml");
		context.start();
		System.err.println("消费服务启动...");
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
