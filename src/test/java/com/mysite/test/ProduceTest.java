package com.mysite.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mysite.producer.ProducerService;

public class ProduceTest {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring-producer.xml");
		context.start();
		System.err.println("生产服务启动...");
		ProducerService ps = context.getBean(ProducerService.class);
		while (true) {
			ps.produce();
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
